import random
import os
import time
print("Rules: Inorder to gain more points, you need not to roll 6&6.\nIf you roll 6&6 you lose all your points. Good luck.")
print("Press enter for rolling")

userBalance = 0

while True:
    userHealth = 1
    userInput = input(">>> ")
    clear = lambda: os.system('cls')
    if userInput.upper() in ['']:
        Nmin = 1
        Nmax = 6
        dice1 = random.randint(Nmin, Nmax)
        dice2 = random.randint(Nmin, Nmax)
        clear()
        time.sleep(0.7)
        print("Rolling.")
        time.sleep(0.3)
        print("Rolling..")
        time.sleep(0.3)
        print("Rolling...\n")
        time.sleep(0.4)
        print("Dice 1: ", dice1)
        print("Dice 2: ", dice2)
        if dice1 == 6 and dice2 == 6:
            print("DEAD !!!")
            userBalance = 0
            print("Balance: ♦" + str(userBalance))
            continue
        else:
            userBalance += 100
            print("Balance: ♦" + str(userBalance) + "\n-------------------------")
    else:
        print("What ?")
