import os
import time

from character import *


clear = lambda: os.system('cls')

class bcolors:
    HEADER = '\33[93m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


tse = "%sTry something else!%s" %(bcolors.FAIL, bcolors.ENDC)


def main():
    print("Chose your class " + bcolors.BOLD + "↓" + bcolors.ENDC + "\n[1] Fighter\n[2] Cleric\n[3] Mage\n" + bcolors.WARNING + "[x] For classes info" +  bcolors.ENDC)
    choice = input(": ")
    if choice == '1':
        Prof = Fighter()
    elif choice == '2':
        Prof = Cleric()
    elif choice == '3':
        from character import Mage
        Prof = Mage()
    elif choice.upper() == 'X':
        time.sleep(0.2)
        clear()
        class_Info()
    else:
        print(tse)
        time.sleep(3)
        clear()
        main()

def class_Info():
    print(bcolors.HEADER + "~ ~ ~ Class Information ~ ~ ~\n" + bcolors.ENDC)
    print(bcolors.WARNING + "Fighter:\n" + bcolors.OKGREEN + "Lorem Ipsum is simply dummy text of the printing and \n"
        "typesetting industry. Lorem Ipsum has been the industry's standard\n"
        "dummy text ever since the 1500s, when an unknown printer took a galley of \n"
        "including versions of Lorem Ipsum\n" + bcolors.ENDC)
    print(bcolors.WARNING + "Clerk:\n" + bcolors.OKGREEN + "Contrary to popular belief, Lorem Ipsum is not simply\n"
        "random text. It has roots in a piece of classical Latin \n"
        "the cites of the word in classical literature, discovered the\n"
        "undoubtable source. L\n" + bcolors.ENDC)
    print(bcolors.WARNING + "Mage:\n" + bcolors.OKGREEN + "random text. It has roots in a piece of classical Latin \n"
        "literature from 45 BC, making it over 2000 years old. Richard\n"
        "undoubtable source. L\n" + bcolors.ENDC)
    print("[x] Back")
    choice = input(": ")
    if choice.upper() == 'X':
        time.sleep(0.2)
        clear()
        main()
    else:
        print(tse)
        time.sleep(1)
        clear()
        class_Info()


def menu():
    player = Player()

    ### Imports
    from battleArena import battle_Arena
    from inventory import inventory
    from shop import shop

    time.sleep(0.3)
    clear()
    print(bcolors.HEADER + "~ ~ ~ MENU ~ ~ ~" + bcolors.ENDC)
    print("\n==========================")
    print("! STATUS ")
    print("| Name: %s |" %(player.name))
    print("| HP: %s | Armor: %s | Level: %s |" %(player.hp, player.armor, player.level))
    print("==========================\n")
    print(bcolors.HEADER + "Selection" + bcolors.ENDC)
    print("⚔[1] Battle Arena")
    print("⚒[2] Shop")
    print("[3] Inventory")
    print("[~ ~ ~ DEVELOPER ~ ~ ~]")
    print("⚠[color] For awailable text color")
    choice = input(str(": "))
    if choice == '1':
        time.sleep(0.3)
        clear()
        battle_Arena()
    elif choice == '2':
        time.sleep(0.3)
        clear()
        shop()
    elif choice == '3':
        time.sleep(0.3)
        clear()
        inventory()
    elif choice == 'color':
        time.sleep(0.3)
        clear()
        print(bcolors.HEADER + "TEXT Text TeXt .HEADER")
        print(bcolors.OKBLUE + "TEXT Text TeXt .OKBLUE")
        print(bcolors.OKGREEN + "TEXT Text TeXt .OKGREEN")
        print(bcolors.WARNING + "TEXT Text TeXt .WARNING")
        print(bcolors.FAIL + "TEXT Text TeXt .FAIL")
        print(bcolors.ENDC + "TEXT Text TeXt .ENDC")
        print(bcolors.BOLD + "TEXT Text TeXt .BOLD")
        print(bcolors.UNDERLINE + "TEXT Text TeXt .UNDERLINE\n\n" + bcolors.ENDC)

        # Shows all colors
        x = 0
        for i in range(24):
            colors = ""
            for j in range(5):
                code = str(x + j)
                colors = colors + "\33[" + code + "m\\33[" + code + "m\033[0m "
            print(colors)
            x = x + 5
    else:
        print(bcolors.FAIL +"ERROR !!!" + bcolors.ENDC)
        time.sleep(1.5)
        menu()






main() #-1

menu() #-2