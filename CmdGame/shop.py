import os

class bcolors:
    HEADER = '\33[93m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

clear = lambda: os.system('cls')

def shop():
    print("~ ~ ~ ⚒ Shop ⚒ ~ ~ ~\n")

    print("Your Balance: ♦274.42\n")

    print("%s[1] %sLong sword -%s| %sCost: ♦1248 %s| %s:Status: %sDamage: 12 %s; %sStamina: 4%s"
          %(bcolors.BOLD, bcolors.OKGREEN, bcolors.ENDC, bcolors.WARNING, bcolors.ENDC,bcolors.FAIL, bcolors.HEADER, bcolors.ENDC, bcolors.HEADER, bcolors.ENDC))
    print("%s[2] %sKatana -----%s| %sCost: ♦3754 %s| %s:Status: %sDamage: 18 %s; %sStamina: 5%s"
          %(bcolors.BOLD, bcolors.OKGREEN, bcolors.ENDC, bcolors.WARNING, bcolors.ENDC, bcolors.FAIL, bcolors.HEADER, bcolors.ENDC, bcolors.HEADER, bcolors.ENDC))
    print("%s[3] %sClaymore ---%s| %sCost: ♦5670 %s| %s:Status: %sDamage: 24 %s; %sStamina: 7%s"
          %(bcolors.BOLD, bcolors.OKGREEN, bcolors.ENDC, bcolors.WARNING, bcolors.ENDC, bcolors.FAIL, bcolors.HEADER, bcolors.ENDC, bcolors.HEADER, bcolors.ENDC))
    print("%s[4] %sDadaoe -----%s| %sCost: ♦7540 %s| %s:Status: %sDamage: 30 %s; %sStamina: 8%s"
          %(bcolors.BOLD, bcolors.OKGREEN, bcolors.ENDC, bcolors.WARNING, bcolors.ENDC, bcolors.FAIL, bcolors.HEADER, bcolors.ENDC, bcolors.HEADER, bcolors.ENDC))
    print("---------------------------------")


    choice = input(str(": "))

    if choice == '':
        clear()
        shop()
    else:
        print("What?")
        clear()
        shop()







shop()